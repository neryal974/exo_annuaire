<? require_once 'DataBase.php';

$query = $db->prepare("SELECT * FROM users WHERE id = :id");

$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

 $query->execute(array(
  ':id' => $id
));

$infos = $query->fetch(PDO::FETCH_ASSOC);

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $first_name = filter_input(INPUT_POST, 'first_name');
    $last_name = filter_input(INPUT_POST, 'last_name');
    $email = filter_input(INPUT_POST, 'email');
    $gender = filter_input(INPUT_POST, 'gender');
    $photo = filter_input(INPUT_POST, 'photo');
    $phone = filter_input(INPUT_POST, 'phone');
    $slogan = filter_input(INPUT_POST, 'slogan');
     
    
    $query = $db->prepare("UPDATE users SET first_name=:first_name, last_name=:last_name, email=:email, gender=:gender, photo=:photo, phone=:phone, slogan=:slogan WHERE id=:id");
    $query->execute([
        ':first_name'=>$first_name,
        ':id'=> $infos['id'],
        ':last_name'=>$last_name,
        ':email'=>$email,
        ':gender'=>$gender,
        ':photo'=>$photo,
        ':phone'=>$phone,
        ':slogan'=>$slogan,

    ]);
    
    echo "<pre>";
     $query->debugDumpParams();
      echo "</pre>";
    }

?>

<html>

<head>
    <title>Rajout</title>
</head>

<body>
     <form method="post"> 
        <label for="first_name">Prénom:</label>
        <input type="text" name="first_name" value="<?= $infos['first_name']?>" required="required">
        <br><br>
        <label for="last_name">Nom de famille:</label>
        <input type="text" name="last_name" value="<?= $infos['last_name']?>" required="required">
        <br><br>
        <label for="email">Email:</label>
        <input type="text" name="email" value="<?= $infos['email']?>" required="required">
        <br><br>
        <label for="gender">Genre:</label>
        <select name="gender" >
            <option value="Male">Homme</option required="required">
            <option value="Female">Femme</option required="required">
        </select>
        <br><br>
        <label for="photo">Photo:</label>
        <input type="url" name="photo" src="" value="<?= $infos['photo']?>" required="required">
        <br><br>
        <label for="phone">Téléphone:</label>
        <input type="tel" name="phone" pattern="[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}" value="<?= $infos['phone']?>" required="required">
        <br><br>
        <label for="slogan">Slogan:</label>
        <input type="text" name="slogan" value="<?= $infos['slogan']?>" required="required">
        </br></br>
        <input type="submit" name="register" value="modifier">
        <a href="index.php">Retour à l'accueil</a>
    </form>
</body>


</html>

