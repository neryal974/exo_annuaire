<?

$query = $db->prepare("SELECT COUNT(*) AS cnt FROM users");
  $query->execute();
  $countAll = $query->fetch(PDO::FETCH_ASSOC);
  
  $query = $db->prepare("SELECT COUNT(*) AS cnt FROM users WHERE gender = 'male'");
  $query->execute();
  $countMale = $query->fetch(PDO::FETCH_ASSOC);

  $query = $db->prepare("SELECT COUNT(*) AS cnt FROM users WHERE gender = 'female'");
  $query->execute();
  $countFemale = $query->fetch(PDO::FETCH_ASSOC); 
  ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Users<?=$countAll['cnt']?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php?gender=male">Hommes<?=$countMale['cnt']?><span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.php?gender=female">Femmes<?=$countFemale['cnt']?></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="add_user.php">Rajout utilisateur</a>
      </li>
    </ul>
  </div>
</nav>