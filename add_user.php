<? require_once 'DataBase.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    $first_name = filter_input(INPUT_POST, 'first_name');
    $last_name = filter_input(INPUT_POST, 'last_name');
    $email = filter_input(INPUT_POST, 'email');
    $gender = filter_input(INPUT_POST, 'gender');
    $photo = filter_input(INPUT_POST, 'photo');
    $phone = filter_input(INPUT_POST, 'phone');
    $slogan = filter_input(INPUT_POST, 'slogan');
    
// utiliser les paramètres nommés 
    $query = $db->prepare("INSERT INTO users(first_name, last_name, email, gender, photo, phone, slogan) VALUES (:first_name, :last_name, :email, :gender, :photo, :phone, :slogan)");
// envoyer les paramètres à la méthode execute
    $result=$query->execute([
        ':first_name'=>$first_name,
        // ':id'=> $infos['id'],
        ':last_name'=>$last_name,
        ':email'=>$email,
        ':gender'=>$gender,
        ':photo'=>$photo,
        ':phone'=>$phone,
        ':slogan'=>$slogan,
    ]);

    var_dump($result);
}


    
?>

<html>

<head>
    <title>Rajout</title>
</head>

<body>
     <form method="post"> <!--"action" pas obligatoire quand données envoyées sur la même page -->
        <label for="first_name">Prénom:</label>
        <input type="text" name="first_name" required="required">
        <br><br>
        <label for="last_name">Nom de famille:</label>
        <input type="text" name="last_name" required="required">
        <br><br>
        <label for="email">Email:</label>
        <input type="text" name="email" required="required">
        <br><br>
        <label for="gender">Genre:</label>
        <select name="gender" required="required">
            <option value="Male">Homme</option>
            <option value="Female">Femme</option>
        </select>
        <br><br>
        <label for="photo">Photo:</label>
        <input type="url" name="photo" required="required">
        <br><br>
        <label for="phone">Téléphone:</label>
        <input type="tel" name="phone" pattern="[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2}" required="required">
        <br><br>
        <label for="slogan">Slogan:</label>
        <input type="text" name="slogan" required="required">
        </br></br>
        <input type="submit" name="register" value="rajouter">
    </form>
    <a href="index.php">Retour à l'accueil</a>
</body>


</html>