<?
require_once 'DataBase.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  <title>Annuaire</title>
</head>

<body>
  <?


$gender = filter_input(INPUT_GET, 'gender');

if ($gender == "male") {
  $sql = ("SELECT * FROM users WHERE gender = 'male'");
} else if ($gender == "female") {
  $sql = ("SELECT * FROM users WHERE gender = 'female'");
} else {
  $sql = ("SELECT * FROM users");
}

  $query = $db->prepare($sql);
  $query->execute();

  $users = $query->fetchAll(PDO::FETCH_ASSOC);

  

  // echo '<pre>';
  // print_r($countAll);
  // print_r($countMale);
  // print_r($countFemale);
  // echo '</pre>';


  require_once 'nav.php';



?>
  <h1>Annuaire</h1>
  <br>


  <div class="row m-auto">
    <?
    foreach ($users as $user) {
      
      // echo "gender = ".$user['gender']." nnnnnnn"; => DEBUG

      $genderColor = ($user['gender'] === 'Male') ? 'blue': 'red' ; ?>
      <div class="card" style="width: 22rem; margin: 4px">
        <img src="<?= $user['photo'] ?>" class="card-img-top" alt="Card image">
        <div class="card-body">
          <h5 style="color:<?=$genderColor?>" class="card-title"><?= $user['first_name']?></h5>
          <p class="card-text"><?= $user['slogan'] ?></p>
          <p class="card-text">E-mail :<?= $user['email'] ?></p>
          <p class="card-text">Téléphone :<?= $user['phone'] ?></p>
          <a href="user.php?id=<?= $user['id'] ?>" class="btn btn-primary">Voir la fiche</a>
          <a href="delete_user.php?id=<?= $user['id'] ?>" class="btn btn-danger">Supprimer</a>
          <a href="update_user.php?id=<?= $user['id'] ?>" class="btn btn-warning">Modifier</a>
        </div>
      </div>
    <? } ?>
  </div>


</body>

</html>