-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 13 mai 2020 à 12:29
-- Version du serveur :  5.7.24
-- Version de PHP : 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `annuaire`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `gender` varchar(256) NOT NULL,
  `photo` varchar(256) NOT NULL,
  `phone` varchar(256) NOT NULL,
  `slogan` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `gender`, `photo`, `phone`, `slogan`) VALUES
(1, 'Luce', 'Mc Mechan', 'lmcmechan0@sun.com', 'Male', 'https://robohash.org/lmcmechan0', '349-120-5545', 'Open-source needs-based capability'),
(2, 'Sandor', 'Tomkin', 'stomkin1@patch.com', 'Male', 'https://robohash.org/stomkin1', '687-810-9097', 'Sharable full-range initiative'),
(3, 'Cassandra', 'Danaher', 'cdanaher2@opera.com', 'Female', 'https://robohash.org/cdanaher2', '799-652-0375', 'Synergistic content-based knowledge user'),
(4, 'Vanda', 'Lawrie', 'vlawrie3@businesswire.com', 'Female', 'https://robohash.org/vlawrie3', '636-681-7144', 'Diverse disintermediate database'),
(5, 'Ches', 'Le Grove', 'clegrove4@nhs.uk', 'Male', 'https://robohash.org/clegrove4', '331-696-2137', 'Up-sized hybrid concept'),
(6, 'Silvan', 'Clemendot', 'sclemendot5@arizona.edu', 'Male', 'https://robohash.org/sclemendot5', '684-666-0604', 'Devolved dedicated leverage'),
(7, 'Maria', 'Christoffersen', 'mchristoffersen6@ehow.com', 'Female', 'https://robohash.org/vhorleyk', '884-465-7898', 'Public-key disintermediate flexibility'),
(8, 'Dalston', 'Braund', 'dbraund7@chron.com', 'Male', 'https://robohash.org/dbraund7', '254-816-6312', 'Cross-platform attitude-oriented framework'),
(9, 'Kay', 'Watting', 'kwatting8@vimeo.com', 'Female', 'https://robohash.org/kwatting8', '340-237-0054', 'Horizontal bi-directional strategy'),
(10, 'Chickie', 'Ainsbury', 'cainsbury9@amazonaws.com', 'Male', 'https://robohash.org/cainsbury9', '434-671-4443', 'Ameliorated maximized firmware'),
(11, 'Rustie', 'Dunkley', 'rdunkleya@google.co.jp', 'Male', 'https://robohash.org/rdunkleya', '772-656-5307', 'Ergonomic executive challenge'),
(12, 'Ansley', 'Bresland', 'abreslandb@paginegialle.it', 'Female', 'https://robohash.org/abreslandb', '575-849-4535', 'Profit-focused neutral benchmark'),
(13, 'Ellis', 'Kelston', 'ekelstonc@umn.edu', 'Male', 'https://robohash.org/ekelstonc', '341-555-0736', 'Configurable zero defect paradigm'),
(14, 'Goddart', 'Courage', 'gcouraged@google.pl', 'Male', 'https://robohash.org/gcouraged', '108-905-3214', 'Object-based logistical task-force'),
(15, 'Terry', 'Whellans', 'twhellanse@typepad.com', 'Male', 'https://robohash.org/twhellanse', '535-326-5401', 'Customer-focused discrete time-frame'),
(16, 'Amby', 'Rookledge', 'arookledgef@state.tx.us', 'Male', 'https://robohash.org/arookledgef', '624-399-3087', 'Assimilated client-driven instruction set'),
(17, 'Neila', 'Ziemsen', 'nziemseng@ezinearticles.com', 'Female', 'https://robohash.org/nziemseng', '839-584-6826', 'Public-key regional alliance'),
(18, 'Leif', 'Klimuk', 'lklimukh@last.fm', 'Male', 'https://robohash.org/lklimukh', '465-305-7264', 'Extended fault-tolerant pricing structure'),
(19, 'Enrica', 'Androlli', 'eandrollii@accuweather.com', 'Female', 'https://robohash.org/eandrollii', '390-257-0005', 'Pre-emptive holistic emulation'),
(20, 'Missy', 'Diamant', 'mdiamantj@joomla.org', 'Female', 'https://robohash.org/mdiamantj', '856-345-6219', 'Team-oriented regional hierarchy'),
(21, 'Venus', 'Horley', 'vhorleyk@apple.com', 'Female', 'https://robohash.org/vhorleyk', '313-618-6684', 'Progressive attitude-oriented data-warehouse'),
(22, 'Cazzie', 'Blinckhorne', 'cblinckhornel@wufoo.com', 'Male', 'https://robohash.org/cblinckhornel', '994-231-0576', 'Operative fresh-thinking strategy'),
(23, 'Alicea', 'Boole', 'aboolem@usa.gov', 'Female', 'https://robohash.org/aboolem', '775-815-3265', 'Advanced bottom-line success'),
(24, 'Ezechiel', 'Baroc', 'ebarocn@gravatar.com', 'Male', 'https://robohash.org/ebarocn', '272-259-4169', 'Team-oriented didactic budgetary management'),
(25, 'Obadias', 'Fancutt', 'ofancutto@jiathis.com', 'Male', 'https://robohash.org/ofancutto', '554-605-1334', 'Profound heuristic extranet');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
